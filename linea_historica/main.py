import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
from sys import argv as comandos

def main():
    # DATOS
    df = pd.read_csv("data.csv", sep=",").sort_values('fnac')
    nacimiento = df['fnac']
    fallecimiento = df['ffall']

    # VISUALIZACIÓN
    fig, ax = plt.subplots(figsize=(15, 10))
    ax.axes.get_yaxis().set_visible(False)
    #fig, ax = plt.subplots()

    cola_fechas = [-6999]
    for i,persona in df.iterrows():
        nom, nac, fal = persona

        # Aplanar el gráfico
        for j,fecha in enumerate(cola_fechas):
            i_nivel = True 
            if nac > fecha:
                i_nivel = False
                nivel = j + 1 # + 1 PARA MEJOR VISUALIZACIÓN
                cola_fechas[j] = fal
                break

        if i_nivel:
            nivel = len(cola_fechas) + 1 # + 1 PARA MEJOR VISUALIZACIÓN
            cola_fechas.append(fal)

        print(f"Nuevo nacimiento: {nac: <15} Nivel: {nivel: <15} Cola: {cola_fechas}")

        arr = mpatches.ConnectionPatch((nac, nivel), (fal, nivel), 'data')
        ax.add_patch(arr)
        ax.annotate(nom, (.5, .5), xycoords=arr, ha='center', va='bottom')

    # Ajustar ancho
    ax.set(xlim=(nacimiento.min()-50, fallecimiento.max()+50), ylim=(0, 30))
    plt.show()
